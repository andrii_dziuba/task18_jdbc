package transform;

import model.Author;
import model.annotations.Column;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TransformerUtils {

    public static <T> T transformTo(ResultSet rs, Class<T> type) {
        Field[] declaredFields = type.getDeclaredFields();
        T t = null;
        try {
            t = type.getConstructor().newInstance();
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }

        for (Field declaredField : declaredFields) {
            if (declaredField.isAnnotationPresent(Column.class)) {
                Column column = declaredField.getAnnotation(Column.class);
                String columnName = column.value();

                Class<?> fieldType = declaredField.getType();
                declaredField.setAccessible(true);
                try {
                    Object value = null;
                    if (fieldType == String.class) {
                        value = rs.getString(columnName);
                    } else if (fieldType == int.class) {
                        value = rs.getInt(columnName);
                    } else if (fieldType == Date.class) {
                        value = rs.getDate(columnName);
                    } else if (fieldType == Author.Genders.class) {
                        value = rs.getString(columnName).contains("female") ? Author.Genders.FEMALE : Author.Genders.MALE;
                    }
                    declaredField.set(t, value);
                } catch (SQLException | IllegalArgumentException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return t;
    }

}
