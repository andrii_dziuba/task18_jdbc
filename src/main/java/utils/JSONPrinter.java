package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.text.SimpleDateFormat;

public class JSONPrinter {

    private static ObjectMapper objectMapper = new ObjectMapper() {{
        setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
    }};

    public static void printPrettyJSON(Object all) {
        try {
            System.out.println(objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(all));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
