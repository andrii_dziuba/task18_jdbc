package utils;

import model.Author;
import model.annotations.Column;
import model.annotations.PrimaryKey;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.Scanner;

import static utils.DefaultScanner.getScanner;

public class ScanUtils {

    public static <T> T scanData(Class<T> type, boolean withId) {
        T instance = null;
        try {
            instance = type.getConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }

        try (Scanner scanner = getScanner()) {
            Field[] fields = type.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Column.class) &&
                        (withId && field.isAnnotationPresent(PrimaryKey.class))) {
                    System.out.println("Enter <" + field.getName() + ">:");
                    scanner.hasNext();
                    String value = scanner.nextLine();
                    field.setAccessible(true);
                    try {
                        if (field.getType() == Date.class) {
                            field.set(instance, Date.valueOf(LocalDate.parse(value)));
                        } else if(field.getType() == int.class) {
                            field.set(instance, Integer.parseInt(value));
                        } else if(field.getType() == Author.Genders.class) {
                            field.set(instance, "female".contains(value) ? Author.Genders.FEMALE : Author.Genders.MALE);
                        } else if(field.getType() == String.class) {
                            field.set(instance, value);
                        }

                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return instance;
    }
}
