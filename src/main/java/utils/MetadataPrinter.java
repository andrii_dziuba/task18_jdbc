package utils;

import configs.DBConnector;
import model.ColumnModel;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MetadataPrinter {

    List<String> tables = new ArrayList<>();

    public DatabaseMetaData getMetadata() throws SQLException {
        Connection connection = DBConnector.getConnection();
        DatabaseMetaData metaData = metaData = connection.getMetaData();
        return metaData;
    }

    public List<String> getTableList() throws SQLException {
        Connection connection = DBConnector.getConnection();
        ResultSet catalogs = getMetadata().getTables(connection.getCatalog(), null, "%", new String[]{"TABLE"});
        List<String> tables = new ArrayList<>();

        while(catalogs.next()) {
            tables.add(catalogs.getString("TABLE_NAME"));
        }
        return tables;
    }

    public List<ColumnModel> getColumns(String table) throws SQLException {
        Connection connection = DBConnector.getConnection();
        ResultSet catalogs = getMetadata().getColumns(connection.getCatalog(), null, table,"%");
        List<ColumnModel> columns = new ArrayList<>();

        List<String> pkList = new ArrayList<>();
        ResultSet primaryKeys = getMetadata().getPrimaryKeys(connection.getCatalog(), null, table);
        while (primaryKeys.next()) {
            pkList.add(primaryKeys.getString("COLUMN_NAME"));
        }

        System.out.println(pkList);

        while(catalogs.next()) {
            String tableName = catalogs.getString("COLUMN_NAME");
            String typeName = catalogs.getString("TYPE_NAME");
            String nullable = catalogs.getString("IS_NULLABLE");
            String autoIncrement = catalogs.getString("IS_AUTOINCREMENT");
            boolean remove = pkList.remove(tableName);
            ColumnModel columnModel = new ColumnModel(tableName, typeName, "NO".equals(nullable), remove, "YES".equals(autoIncrement));
            columns.add(columnModel);
        }
        return columns;
    }
}
