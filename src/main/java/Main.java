import model.Author;
import model.ColumnModel;
import service.AuthorService;
import service.BookService;
import utils.MetadataPrinter;
import utils.Printable;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static utils.DefaultScanner.getScanner;
import static utils.ScanUtils.scanData;

public class Main {

    private AuthorService authorService = new AuthorService();
    private BookService bookService = new BookService();

    private Map<String, Printable> menuMap = new LinkedHashMap<>();

    {
        menuMap.put("1", this::printMetadata);
        menuMap.put("2", this::printData);
        menuMap.put("3", this::createAuthor);
        menuMap.put("4", this::createBook);

    }

    private void createBook() {

    }

    private void createAuthor() {
        Author author = scanData(Author.class, false);
        authorService.createAuthor(author);
    }


    void printMenu() {
        System.out.println("1 - print metadata\n" +
                "2 - print data\n" +
                "3 - create new author\n" +
                "4 - create new book\n");
        try (Scanner scanner = getScanner()) {
            while (scanner.hasNext()) {
                System.out.println("1 - print metadata\n" +
                        "2 - print data\n" +
                        "3 - create new author\n" +
                        "4 - create new book\n");

                String next = scanner.nextLine();
                Printable printable = menuMap.get(next);
                if (printable == null) break;
                printable.perform();
            }
        }
    }

    void printMetadata() {
        try {
            MetadataPrinter metadataPrinter = new MetadataPrinter();
            List<String> tableList = metadataPrinter.getTableList();
            tableList.forEach(i -> {
                System.out.println(i);
                try {
                    List<ColumnModel> columns = metadataPrinter.getColumns(i);

                    for (ColumnModel column : columns) {
                        System.out.println(column);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();

        }
    }

    void printData() {
        authorService.printAuthors();
        bookService.printBooks();
    }

    public static void main(String[] args) {
        Main main = new Main();
        main.printMenu();
    }
}
