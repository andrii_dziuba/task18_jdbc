package dao;

import configs.DBConnector;
import model.Entity;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public interface GeneralDAO {

    String DELETE = "DELETE FROM authors WHERE authorId=?";

    List<? extends Entity> findAll();
    Entity findById(int id);

    Integer create(Entity entity);
    Integer update(Entity toUpdate);

    default Integer delete(int id) {
        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(DELETE);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            return delete;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
