package dao.impl;

import configs.DBConnector;
import dao.AuthorDAO;
import model.Author;
import model.Entity;
import transform.TransformerUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AuthorDAOImpl implements AuthorDAO {

    private static final String SELECT_ALL = "SELECT * FROM authors";
    private static final String SELECT_BY_ID = "SELECT * FROM authors WHERE authorId=?";
    private static final String CREATE = "INSERT INTO authors (aName, gender, birth) VALUES (?, ?, ?)";
    private static final String UPDATE = "UPDATE authors SET aName=?, gender=?, birth=? WHERE authorId=?";
    private static AuthorDAOImpl instance = null;

    private AuthorDAOImpl(){}

    public static AuthorDAO getAuthorDAO() {
        if(instance == null) {
            instance = new AuthorDAOImpl();
        }
        return instance;
    }

    public List<Author> findAll() {
        List<Author> list = new ArrayList<>();

        try {
            Statement statement = DBConnector.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL);
            while (resultSet.next()) {
                list.add(TransformerUtils.transformTo(resultSet, Author.class));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public Entity findById(int id) {
        Author author = null;
        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(SELECT_BY_ID);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                author = TransformerUtils.transformTo(resultSet, Author.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return author;
    }

    public Integer create(Entity entity) {
        if(!(entity instanceof Author)) throw new IllegalArgumentException();
        Author author = (Author) entity;
        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(CREATE);
            statement.setString(1, author.getAuthorName());
            statement.setString(2, author.getGender() == Author.Genders.FEMALE ? "female" : "male");
            statement.setDate(3, author.getBirth());
            int update = statement.executeUpdate();
            return update;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Integer update(Entity toUpdate) {
        if(!(toUpdate instanceof Author)) throw new IllegalArgumentException();
        Author author = (Author) toUpdate;
        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(UPDATE);
            statement.setString(1, author.getAuthorName());
            statement.setString(2, author.getGender() == Author.Genders.FEMALE ? "female" : "male");
            statement.setDate(3, author.getBirth());
            statement.setInt(4, author.getId());
            int update = statement.executeUpdate();
            return update;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
