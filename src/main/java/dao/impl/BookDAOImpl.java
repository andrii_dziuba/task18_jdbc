package dao.impl;

import configs.DBConnector;
import dao.BookDAO;
import model.Author;
import model.Book;
import model.Entity;
import transform.TransformerUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BookDAOImpl implements BookDAO {
    private static final String SELECT_ALL = "SELECT * FROM books";
    private static final String SELECT_BY_ID = "SELECT * FROM books WHERE bookId=?";
    private static final String CREATE = "INSERT INTO books (bName, published, genre, rating) VALUES (?, ?, ?, ?)";
    private static final String UPDATE = "UPDATE books SET bName=?, published=?, genre=?, rating=? WHERE bookId=?";

    private static BookDAOImpl instance = null;

    private BookDAOImpl() {}

    public static BookDAO getBookDAO() {
        if(instance == null) {
            instance = new BookDAOImpl();
        }
        return instance;
    }

    public List<Book> findAll() {
        List<Book> list = new ArrayList<>();

        try {
            Statement statement = DBConnector.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL);
            while (resultSet.next()) {
                list.add(TransformerUtils.transformTo(resultSet, Book.class));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public Entity findById(int id) {
        Book book = null;
        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(SELECT_BY_ID);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                book = TransformerUtils.transformTo(resultSet, Book.class);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return book;
    }

    public Integer create(Entity entity) {
        if(!(entity instanceof Book)) throw new IllegalArgumentException();
        Book book = (Book) entity;
        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(CREATE);
            statement.setString(1, book.getBookName());
            statement.setDate(2, book.getPublishDate());
            statement.setString(3, book.getGenre());
            statement.setInt(4, book.getRating());
            int update = statement.executeUpdate();
            return update;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Integer update(Entity toUpdate) {
        if(!(toUpdate instanceof Book)) throw new IllegalArgumentException();
        Book book = (Book) toUpdate;
        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(UPDATE);
            statement.setString(1, book.getBookName());
            statement.setDate(2, book.getPublishDate());
            statement.setString(3, book.getGenre());
            statement.setInt(4, book.getRating());
            statement.setInt(5, book.getId());
            int update = statement.executeUpdate();
            return update;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
