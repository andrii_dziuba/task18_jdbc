package dao.impl;

import configs.DBConnector;
import dao.AuthorDAO;
import dao.BookDAO;
import dao.GeneralDAO;
import model.Author;
import model.AuthorBookRelation;
import model.Book;
import model.Entity;
import transform.TransformerUtils;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AuthorBookRelationDAOImpl implements GeneralDAO {

    private static final String SELECT_ALL = "SELECT * FROM author_book";
    private static final String GET_ALL_AUTHOR_BOOKS = "SELECT bookId FROM author_book WHERE authorId=?";
    private static final String GET_ALL_BOOK_AUTHORS = "SELECT authorId FROM author_book WHERE bookId=?";


    private static final String CREATE = "INSERT INTO author_book (bookId, authorId) VALUES (?, ?)";
    private static final String UPDATE = "UPDATE author_book SET bookId=?, authorId=? WHERE id=?";

    private BookDAO bookDAO = BookDAOImpl.getBookDAO();
    private AuthorDAO authorDAO = AuthorDAOImpl.getAuthorDAO();

    @Override
    public Entity findById(int id) {
        throw new UnsupportedOperationException();
    }

    public List<AuthorBookRelation> findAll() {
        List<AuthorBookRelation> list = new ArrayList<>();

        try {
            Statement statement = DBConnector.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL);
            while (resultSet.next()) {
                list.add(TransformerUtils.transformTo(resultSet, AuthorBookRelation.class));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Book> findAllAuthorBooks(int id) {
        List<Book> list = new ArrayList<>();

        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(GET_ALL_AUTHOR_BOOKS);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Entity book = bookDAO.findById(resultSet.getInt("bookId"));
                list.add((Book) book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<Author> findAllBookAuthors(int id) {
        List<Author> list = new ArrayList<>();

        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(GET_ALL_BOOK_AUTHORS);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Entity author = authorDAO.findById(resultSet.getInt("authorId"));
                list.add((Author) author);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public Integer create(Entity entity) {
        if(!(entity instanceof AuthorBookRelation)) throw new IllegalArgumentException();
        AuthorBookRelation relation = (AuthorBookRelation) entity;
        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(CREATE);
            statement.setInt(1, relation.getBookId());
            statement.setInt(2, relation.getAuthorId());
            int update = statement.executeUpdate();
            return update;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Integer update(Entity toUpdate) {
        if(!(toUpdate instanceof AuthorBookRelation)) throw new IllegalArgumentException();
        AuthorBookRelation relation = (AuthorBookRelation) toUpdate;
        try {
            PreparedStatement statement = DBConnector.getConnection().prepareStatement(UPDATE);
            statement.setInt(1, relation.getBookId());
            statement.setInt(2, relation.getAuthorId());
            statement.setInt(3, relation.getId());
            int update = statement.executeUpdate();
            return update;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
