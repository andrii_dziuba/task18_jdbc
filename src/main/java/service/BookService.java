package service;

import dao.BookDAO;
import dao.impl.AuthorBookRelationDAOImpl;
import dao.impl.BookDAOImpl;
import model.Author;
import model.Book;
import model.Entity;
import utils.JSONPrinter;

import java.util.List;

import static utils.ScanUtils.scanData;

public class BookService {

    private BookDAO bookDAO = BookDAOImpl.getBookDAO();
    private AuthorBookRelationDAOImpl relationDAO = new AuthorBookRelationDAOImpl();

    public void printBooks() {
        List<? extends Entity> all = bookDAO.findAll();
        all.forEach(i -> {
            Book book = (Book) i;
            List<Author> allBookAuthors = relationDAO.findAllBookAuthors(book.getId());
            book.setAuthors(allBookAuthors);
        });
        JSONPrinter.printPrettyJSON(all);
    }

    public void createBook() {
        Book book = scanData(Book.class, false);
        bookDAO.create(book);
    }

    public void updateBook() {
        Book book = scanData(Book.class, true);
        bookDAO.update(book);
    }

    public void deleteBook() {
        Book book = scanData(Book.class, true);
        bookDAO.delete(book.getId());
    }
}
