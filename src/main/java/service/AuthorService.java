package service;

import dao.AuthorDAO;
import dao.impl.AuthorBookRelationDAOImpl;
import dao.impl.AuthorDAOImpl;
import model.Author;
import model.Book;
import model.Entity;
import utils.JSONPrinter;

import java.util.List;

import static utils.ScanUtils.scanData;

public class AuthorService {

    private AuthorDAO authorDAO = AuthorDAOImpl.getAuthorDAO();
    private AuthorBookRelationDAOImpl relationDAO = new AuthorBookRelationDAOImpl();

    public void printAuthors() {
        List<? extends Entity> all = authorDAO.findAll();
        all.forEach(i -> {
            Author author = (Author) i;
            List<Book> allAuthorBooks = relationDAO.findAllAuthorBooks(author.getId());
            author.setBooks(allAuthorBooks);
        });
        JSONPrinter.printPrettyJSON(all);
    }

    public void createAuthor(Author author) {
        authorDAO.create(author);
    }

    public void updateAuthor() {
        Author author = scanData(Author.class, true);
        authorDAO.update(author);
    }

    public void deleteAuthor() {
        Author author = scanData(Author.class, true);
        authorDAO.delete(author.getId());
    }
}
