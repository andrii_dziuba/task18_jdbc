package model;

import lombok.Data;
import model.annotations.Column;
import model.annotations.PrimaryKey;
import model.annotations.Table;

import java.sql.Date;
import java.util.List;

@Data
@Table("books")
public class Book extends Entity {

    @PrimaryKey
    @Column("bookId")
    private int id;
    @Column("bName")
    private String bookName;
    @Column("published")
    private Date publishDate;
    @Column("genre")
    private String genre;
    @Column("rating")
    private int rating;

    private List<Author> authors;
}
