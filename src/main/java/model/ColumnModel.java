package model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ColumnModel {

    private String columnName;
    private String typeName;
    private boolean isPrimaryKey;
    private boolean isNullable;
    private boolean isAutoIncrement;

    @Override
    public String toString() {
        String format = "%-15s  %-12s  %-12s  %-8s  %s";
        return String.format(format, columnName,
                typeName,
                (isPrimaryKey  ? "PRIMARY_KEY" : ""),
                (isNullable ? "NULLABLE" : "NOT_NULL"),
                (isAutoIncrement ? "AI" : ""));

    }
}
