package model;

import lombok.Data;
import model.annotations.Column;
import model.annotations.PrimaryKey;
import model.annotations.Table;

import java.sql.Date;
import java.util.List;
import java.util.Set;

@Data
@Table("authors")
public class Author extends Entity {

    @PrimaryKey
    @Column("authorId")
    private int id;
    @Column("aName")
    private String authorName;
    @Column("gender")
    private Genders gender;
    @Column("birth")
    private Date birth;

    private List<Book> books;

    public enum Genders {
        MALE, FEMALE;
    }

}
