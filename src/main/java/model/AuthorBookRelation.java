package model;

import lombok.Data;
import model.annotations.Column;
import model.annotations.PrimaryKey;
import model.annotations.Table;

@Data
@Table("author_book")
public class AuthorBookRelation extends Entity {

    @PrimaryKey
    @Column("id")
    private int id;
    @Column("bookId")
    private int bookId;
    @Column("authorId")
    private int authorId;
}
